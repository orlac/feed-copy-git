var Promise = require('promise');
var multiparty = require('multiparty');
var fs = require("fs");
module.exports = {

	getService: function(opts){

		opts = opts || {};
		this.maxSize = opts.maxSize || 5 * 1024 * 1024; //5MB
		this.supportMimeTypes = opts.supportMimeTypes || ['image/jpg', 'image/jpeg', 'image/png'];
		this.uploadPath = opts.uploadPath;
		this.fileName = opts.fileName;
		console.log('maxSize', this.maxSize)
		var me = this;

	    this.run = function(req){
	    	
			return new Promise(function (resolve, reject) {

				var form = new multiparty.Form();
			    //здесь будет храниться путь с загружаемому файлу, его тип и размер
			    var uploadFile = {uploadPath: '', type: '', size: 0,  name: ''};
			    //массив с ошибками произошедшими в ходе загрузки файла
			    var errors = [];


			    var _on_error = function(err){
			    	console.log('_on_error', uploadFile, errors, err); 
			        if(fs.existsSync(uploadFile.path)) {
			            //если загружаемый файл существует удаляем его
			            fs.unlinkSync(uploadFile.path);
			            console.log('error');
			        }
			        reject(err);
			    };

			    var _on_close = function() {
			    	console.log('_on_close', uploadFile, errors); 
			        //если нет ошибок и все хорошо
			        if(errors.length == 0) {
			            //сообщаем что все хорошо
			            resolve(uploadFile);
			        }
			        else {
			            if(fs.existsSync(uploadFile.path)) {
			                //если загружаемый файл существует удаляем его
			                fs.unlinkSync(uploadFile.path);
			            }
			            //сообщаем что все плохо и какие произошли ошибки
			            console.log('reject', errors);
			            reject(errors);
			        }
			    };

			    var _on_part = function(part) {
			    	
			        //читаем его размер в байтах
			        uploadFile.size = part.byteCount;
			        //читаем его тип
			        uploadFile.type = part.headers['content-type'];
			        uploadFile.name = me.fileName + '.'+ ( part.filename.split('.').pop() ) ;
			        //путь для сохранения файла
			        uploadFile.path = me.uploadPath+'/' + uploadFile.name;

			        //проверяем размер файла, он не должен быть больше максимального размера
			        if(uploadFile.size > me.maxSize) {
			            errors.push('File size is ' + uploadFile.size + '. Limit is' + (me.maxSize / 1024 / 1024) + 'MB.');
			        }

			        //проверяем является ли тип поддерживаемым
			        if(me.supportMimeTypes.indexOf(uploadFile.type) == -1) {
			            errors.push('Unsupported mimetype ' + uploadFile.type);
			        }

			        //если нет ошибок то создаем поток для записи файла
			        if(errors.length == 0) {
			            var out = fs.createWriteStream(uploadFile.path);
			            part.pipe(out);
			        }
			        else {
			            //пропускаем
			            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
			            part.resume();
			            _on_close();
			        }
			    };

			     //если произошла ошибка
			    form.on('error', _on_error);

			    form.on('close', _on_close);

				// при поступление файла
			    form.on('part', _on_part);

				// парсим форму
				console.log('form.parse', req.files);
	    		form.parse(req);
			});
		}


	} 

	
        
}