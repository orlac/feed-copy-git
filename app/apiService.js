// config = require('./config/config'),
var VK = require('vksdk');
var Facebook = require('facebook-node-sdk');
//var async = require('async-q');
var async = require('async');
var _ = require('lodash');
var Promise = require('promise');
var moment = require('moment'); 
var timeZoneService = require('./timeZoneService.js'); 
var userService = require('./userService.js'); 

var _ = require('lodash');
//var moment = require('moment-timezone');

module.exports = function(db, config){

	var api = require('./api.js')(config); 
	var vk = api.vk;
	var facebook = api.facebook;
	var fbRequest = api.fbRequest;
	var vkRequest = api.vkRequest;

	/**
	//     POST
	*/

	var _post_feed = function(feed, cb){

		var _log_post = function(gr, pid){
			return new Promise(function (resolve, reject) {
				async.series([
				    function(callback){
				        db.user_feeds_posts.create({
							user_id: feed.user_id,
							chid: feed.chid,
							fid: feed.id,
							gid: gr.id,
							type: gr.type,
							pid: pid
						}).then(function(m){
							callback(null, m);
						}).catch(function(e){
							callback(e);
						});
				    },
				    function(callback){
				        db.user_feeds.update({
							publishedOn: db.sequelize.fn('NOW'),
						}, {
		                    where: { id : feed.id }
		                }).then(function(m){
							callback(null, m);
						}).catch(function(e){
							callback(e);
						});
				    }
				],
				// optional callback
				function(err, results){
				    if (err) {
				    	reject( err );
		            } else {
		                resolve(results);
		            }
				});	
			});
		}

		async.map(feed.gids, 
			function(gid, _cb){
				switch(gid.type){
					case 'vk':
						_vk_post_feed(feed, gid,  _cb);
					break
					case 'facebook':
						_facebook_post_feed(feed, gid, _cb).then(function(res){
							console.log('response', res);
							 // _cb(null, res.id);
							_log_post(gid, res.id).then(function(log){
								_cb(null, res.id);
							}).then(function(err){
								_cb(err);	
							});
						}).catch(function(err){
							_cb(err);
						});				
					break;
					default:
						_cb( new Error('post type not found') );
				}
			}, 
			cb
		);	
	}

	var _vk_post_feed = function(feed, gid, cb){
		//console.log('_vk_post_feed', feed.id);
		cb(null, feed.publicOn);
		
	}

	var _facebook_post_feed = function(feed, gid){
		console.log('_facebook_post_feed', feed.id);
		return db.user_auth.getUserServiceByUid(feed.user_id, gid.type).then(function(user_auth){
			facebook.setAccessToken(user_auth.token);
			console.log('gid', gid);
			return new Promise(function (resolve, reject) {

				if(feed.img){
					//todo send source img
					feed.getLoadImgUrl().then(function(uri){
						// console.log(uri);
						fbRequest(facebook, '/'+Math.abs(gid.id)+'/photos', 'POST', {
							url: uri,
							// url: 'http://nodejs.org/images/roadshow-promo.png',
							message: feed.message
						})
						.then(function(response){
							if(response.id){
								resolve(response);
							}else{
								reject(response);	
							}
						})
						.catch(function(err){
							reject(err);
						});
					}).catch(function(err){
						reject(err);
					});

				}else{
					fbRequest(facebook, '/'+Math.abs(gid.id)+'/feed', 'POST', {
						message: feed.message
					})
					.then(function(response){
						if(response.id){
							resolve(response);
						}else{
							reject(response);	
						}
					})
					.catch(function(err){
						reject(err);
					});	
				}


				
			});
			// return fbRequest(facebook, '/'+gid.id+'/feed', 'POST', {
			// 	message: feed.message
			// })
			// /*.then(function(response){
				
			// 	return response;		
			// })*/;	
		});
		
		
	}

	/**
	//  DELETE
	*/

	var _vk_delete_feed = function(){};

	var _facebook_delete_feed = function(feed, feed_post){
		console.log('_facebook_delete_feed', feed.id);
		return db.user_auth.getUserServiceByUid(feed.user_id, feed_post.type).then(function(user_auth){
			facebook.setAccessToken(user_auth.token);
			
			return new Promise(function (resolve, reject) {
				
				fbRequest(facebook, '/'+feed_post.pid, 'DELETE' )
					.then(function(response){
						if(response.success){
							resolve(response);
						}else{
							reject(response);	
						}
					})
					.catch(function(err){
						reject(err);
					});	
			});	
		});
	};

	var _delete_feed = function(feed, cb){

		var _log_post = function(){
			return db.user_feeds.update({
					deletedOn: db.sequelize.fn('NOW'),
				}, {
	                where: { id : feed.id }
	            });
		};

		db.user_feeds_posts.findAll({
			where: {
				fid: feed.id
			}
		}).then(function(feeds_posts){
			async.map(feeds_posts, 
				function(feed_post, _cb){
					switch(feed_post.type){
						case 'vk':
							_vk_delete_feed(feed, feed_post);
						break
						case 'facebook':
							_facebook_delete_feed(feed, feed_post).then(function(res){
								console.log('response', res);
								_log_post().then(function(log){
									_cb(null, feed_post.pid);
								}).catch(function(err){
									_cb(err);	
								});
							}).catch(function(err){
								_cb(err);
							});				
						break;
						default:
							_cb( new Error('post type not found') );
					}
				}, 
				cb
			);	
		}).catch(function(err){
			cb(err);
		});
	}

	var s = function(db){
		
		this.postFeeds = function(models){
			return new Promise(function (resolve, reject) {
				async.map(models, 
					_post_feed, 
					function(err, results){
						if(err){
							reject(err);
						}else{
							resolve(results);
						}
					}
				);	
			});
		}

		this.deleteFeeds = function(models){
			return new Promise(function (resolve, reject) {
				async.map(models, 
					_delete_feed, 
					function(err, results){
						if(err){
							reject(err);
						}else{
							resolve(results);
						}
					}
				);	
			});
		}

		this.vkRequest = vkRequest;
		this.fbRequest = fbRequest;
		
	};

	return new s(db);	
};