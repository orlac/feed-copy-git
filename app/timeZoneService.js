var data = [{
    "label": "(GMT-11:00) Американское Самоа / American Samoa",
    "value": "-11:00"
}, {
    "label": "(GMT-11:00) Международная линия перемены дат Запад / International Date Line West",
    "value": "-11:00"
}, {
    "label": "(GMT-11:00) Мидуэй / Midway Island",
    "value": "-11:00"
}, {
    "label": "(GMT-10:00) Гаваи / Hawaii",
    "value": "-10:00"
}, {
    "label": "(GMT-09:00) Аляска / Alaska",
    "value": "-09:00"
}, {
    "label": "(GMT-08:00) Тихоокеанское время (США и Канада) / Pacific Time (US & Canada)",
    "value": "-08:00"
}, {
    "label": "(GMT-08:00) Тихуана / Tijuana",
    "value": "-08:00"
}, {
    "label": "(GMT-07:00) Аризона / Arizona",
    "value": "-07:00"
}, {
    "label": "(GMT-07:00) Чихуахуа / Chihuahua",
    "value": "-07:00"
}, {
    "label": "(GMT-07:00) Мазатлан / Mazatlan",
    "value": "-07:00"
}, {
    "label": "(GMT-07:00) Горное время (США и Канада) / Mountain Time (US & Canada)",
    "value": "-07:00"
}, {
    "label": "(GMT-06:00) Центральная Америка / Central America",
    "value": "-06:00"
}, {
    "label": "(GMT-06:00) Центральное время (США и Канада) / Central Time (US & Canada)",
    "value": "-06:00"
}, {
    "label": "(GMT-06:00) Гвадалахара / Guadalajara",
    "value": "-06:00"
}, {
    "label": "(GMT-06:00) Мехико / Mexico City",
    "value": "-06:00"
}, {
    "label": "(GMT-06:00) Монтеррей / Monterrey",
    "value": "-06:00"
}, {
    "label": "(GMT-06:00) Саскачеван / Saskatchewan",
    "value": "-06:00"
}, {
    "label": "(GMT-05:00) Богота / Bogota",
    "value": "-05:00"
}, {
    "label": "(GMT-05:00) Центральная Америка / Eastern Time (US & Canada)",
    "value": "-05:00"
}, {
    "label": "(GMT-05:00) Индиана (восток) / Indiana (East)",
    "value": "-05:00"
}, {
    "label": "(GMT-05:00) Лима / Lima",
    "value": "-05:00"
}, {
    "label": "(GMT-05:00) Кито / Quito",
    "value": "-05:00"
}, {
    "label": "(GMT-04:30) Каракас / Caracas",
    "value": "-04:30"
}, {
    "label": "(GMT-04:00) Атлантическое время (Канада) / Atlantic Time (Canada)",
    "value": "-04:00"
}, {
    "label": "(GMT-04:00) Джорджтаун / Georgetown",
    "value": "-04:00"
}, {
    "label": "(GMT-04:00) Ла-Пас / La Paz",
    "value": "-04:00"
}, {
    "label": "(GMT-04:00) Сантьяго / Santiago",
    "value": "-04:00"
}, {
    "label": "(GMT-03:30) Ньюфаундленд / Newfoundland",
    "value": "-03:30"
}, {
    "label": "(GMT-03:00) Бразилиа / Brasilia",
    "value": "-03:00"
}, {
    "label": "(GMT-03:00) Буэнос-Айрес / Buenos Aires",
    "value": "-03:00"
}, {
    "label": "(GMT-03:00) Гренландия / Greenland",
    "value": "-03:00"
}, {
    "label": "(GMT-03:00) Montevideo / Montevideo",
    "value": "-03:00"
}, {
    "label": "(GMT-02:00) Средне-атлантическое / Mid-Atlantic",
    "value": "-02:00"
}, {
    "label": "(GMT-01:00) Азорские острова / Azores",
    "value": "-01:00"
}, {
    "label": "(GMT-01:00) Кабо-Верде. / Cape Verde Is.",
    "value": "-01:00"
}, {
    "label": "(GMT+00:00) Касабланка / Casablanca",
    "value": "+00:00"
}, {
    "label": "(GMT+00:00) Дублин / Dublin",
    "value": "+00:00"
}, {
    "label": "(GMT+00:00) Эдинбург / Edinburgh",
    "value": "+00:00"
}, {
    "label": "(GMT+00:00) Лиссабон / Lisbon",
    "value": "+00:00"
}, {
    "label": "(GMT+00:00) Лондон / London",
    "value": "+00:00"
}, {
    "label": "(GMT+00:00) Монровия / Monrovia",
    "value": "+00:00"
}, {
    "label": "(GMT+00:00) UTC / UTC",
    "value": "+00:00"
}, {
    "label": "(GMT+01:00) Амстердам / Amsterdam",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Белград / Belgrade",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Берлин / Berlin",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Берн / Bern",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Братислава / Bratislava",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Брюссель / Brussels",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Будапешт / Budapest",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Копенгаген / Copenhagen",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Любляна / Ljubljana",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Мадрид / Madrid",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Париж / Paris",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Прага / Prague",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Рим / Rome",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Сараево / Sarajevo",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Скопье / Skopje",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Стокгольм / Stockholm",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Вена / Vienna",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Варшава / Warsaw",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Западной и Центральной Африки / West Central Africa",
    "value": "+01:00"
}, {
    "label": "(GMT+01:00) Загреб / Zagreb",
    "value": "+01:00"
}, {
    "label": "(GMT+02:00) Афины / Athens",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Бухарест / Bucharest",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Каир / Cairo",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Хараре / Harare",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Хельсинки / Helsinki",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Стамбул / Istanbul",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Иерусалим / Jerusalem",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Киев / Kyiv",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Претория / Pretoria",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Рига / Riga",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) София / Sofia",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Таллин / Tallinn",
    "value": "+02:00"
}, {
    "label": "(GMT+02:00) Вильнюс / Vilnius",
    "value": "+02:00"
}, {
    "label": "(GMT+03:00) Багдад / Baghdad",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Кувейт / Kuwait",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Минск / Minsk",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Москва / Moscow",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Найроби / Nairobi",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Рияд / Riyadh",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Санкт-Петербург / St. Petersburg",
    "value": "+03:00"
}, {
    "label": "(GMT+03:00) Волгоград / Volgograd",
    "value": "+03:00"
}, {
    "label": "(GMT+03:30) Тегеран / Tehran",
    "value": "+03:30"
}, {
    "label": "(GMT+04:00) Абу-Даби / Abu Dhabi",
    "value": "+04:00"
}, {
    "label": "(GMT+04:00) Баку / Baku",
    "value": "+04:00"
}, {
    "label": "(GMT+04:00) Мускат / Muscat",
    "value": "+04:00"
}, {
    "label": "(GMT+04:00) Тбилиси / Tbilisi",
    "value": "+04:00"
}, {
    "label": "(GMT+04:00) Ереван / Yerevan",
    "value": "+04:00"
}, {
    "label": "(GMT+04:30) Кабул / Kabul",
    "value": "+04:30"
}, {
    "label": "(GMT+05:00) Екатеринбург / Ekaterinburg",
    "value": "+05:00"
}, {
    "label": "(GMT+05:00) Исламабад / Islamabad",
    "value": "+05:00"
}, {
    "label": "(GMT+05:00) Карачи / Karachi",
    "value": "+05:00"
}, {
    "label": "(GMT+05:00) Ташкент / Tashkent",
    "value": "+05:00"
}, {
    "label": "(GMT+05:30) Ченнаи / Chennai",
    "value": "+05:30"
}, {
    "label": "(GMT+05:30) Калькутта / Kolkata",
    "value": "+05:30"
}, {
    "label": "(GMT+05:30) Мумбаи / Mumbai",
    "value": "+05:30"
}, {
    "label": "(GMT+05:30) Нью-Дели / New Delhi",
    "value": "+05:30"
}, {
    "label": "(GMT+05:30) Шри-Яварденапура / Sri Jayawardenepura",
    "value": "+05:30"
}, {
    "label": "(GMT+05:45) Катманду / Kathmandu",
    "value": "+05:45"
}, {
    "label": "(GMT+06:00) Алматы / Almaty",
    "value": "+06:00"
}, {
    "label": "(GMT+06:00) Астана / Astana",
    "value": "+06:00"
}, {
    "label": "(GMT+06:00) Дакка / Dhaka",
    "value": "+06:00"
}, {
    "label": "(GMT+06:00) Новосибирск / Novosibirsk",
    "value": "+06:00"
}, {
    "label": "(GMT+06:00) Урумчи / Urumqi",
    "value": "+06:00"
}, {
    "label": "(GMT+06:30) Рангун / Rangoon",
    "value": "+06:30"
}, {
    "label": "(GMT+07:00) Бангкок / Bangkok",
    "value": "+07:00"
}, {
    "label": "(GMT+07:00) Ханой / Hanoi",
    "value": "+07:00"
}, {
    "label": "(GMT+07:00) Джакарта / Jakarta",
    "value": "+07:00"
}, {
    "label": "(GMT+07:00) Красноярск / Krasnoyarsk",
    "value": "+07:00"
}, {
    "label": "(GMT+08:00) Пекин / Beijing",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Чунцин / Chongqing",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Гонконг / Hong Kong",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Иркутск / Irkutsk",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Куала-Лумпур / Kuala Lumpur",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Перт / Perth",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Сингапур / Singapore",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Тайбэй / Taipei",
    "value": "+08:00"
}, {
    "label": "(GMT+08:00) Ulaanbaatar / Ulaanbaatar",
    "value": "+08:00"
}, {
    "label": "(GMT+09:00) Осака / Osaka",
    "value": "+09:00"
}, {
    "label": "(GMT+09:00) Саппоро / Sapporo",
    "value": "+09:00"
}, {
    "label": "(GMT+09:00) Сеул / Seoul",
    "value": "+09:00"
}, {
    "label": "(GMT+09:00) Токио / Tokyo",
    "value": "+09:00"
}, {
    "label": "(GMT+09:00) Якутск / Yakutsk",
    "value": "+09:00"
}, {
    "label": "(GMT+09:30) Аделаида / Adelaide",
    "value": "+09:30"
}, {
    "label": "(GMT+09:30) Дарвин / Darwin",
    "value": "+09:30"
}, {
    "label": "(GMT+10:00) Брисбен / Brisbane",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Канберра / Canberra",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Гуам / Guam",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Хобарт / Hobart",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Магадан / Magadan",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Мельбурн / Melbourne",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Порт-Морсби / Port Moresby",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Сидней / Sydney",
    "value": "+10:00"
}, {
    "label": "(GMT+10:00) Владивосток / Vladivostok",
    "value": "+10:00"
}, {
    "label": "(GMT+11:00) Новая Каледония / New Caledonia",
    "value": "+11:00"
}, {
    "label": "(GMT+11:00) Соломоновы острова. / Solomon Is.",
    "value": "+11:00"
}, {
    "label": "(GMT+12:00) Окленд / Auckland",
    "value": "+12:00"
}, {
    "label": "(GMT+12:00) Фиджи / Fiji",
    "value": "+12:00"
}, {
    "label": "(GMT+12:00) Камчатка / Kamchatka",
    "value": "+12:00"
}, {
    "label": "(GMT+12:00) Маршалловы о-ва / Marshall Is.",
    "value": "+12:00"
}, {
    "label": "(GMT+12:00) Веллингтон / Wellington",
    "value": "+12:00"
}, {
    "label": "(GMT+12:45) Chatham Is. / Chatham Is.",
    "value": "+12:45"
}, {
    "label": "(GMT+13:00) Нукуалофа / Nuku'alofa",
    "value": "+13:00"
}, {
    "label": "(GMT+13:00) Самоа / Samoa",
    "value": "+13:00"
}, {
    "label": "(GMT+13:00) Токелау / Tokelau Is.",
    "value": "+13:00"
}];

var _res = {};
for(var i in data){
    if(!_res[data[i].value ]){
        _res[data[i].value ] = [];
        _res[data[i].value ].push( data[i].label ) ;
    }
    // _res[data[i].value ].push( data[i].label ) ;
}
var __res = [];
for(var val in _res){
    __res.push({
        value: val,
        label: _res[val].join(', ')
    });
}

module.exports = {
	getList: function(){
		return __res;
	}
}