var express = require('express'),
  router = express.Router(),
  //marko = require('marko'),
  db = require('../models');
var passport = require('passport');  
var config = require('../../config/config')
var acl = require('../aclService');
var userService = require('../userService')(db, config);  

module.exports = function (app) {
  app.use('/auth', router);
};

router.get('/logout', function (req, res, next) {
    req.logout();
    res.redirect('/');
});

router.get('/vk',
    passport.authenticate('vk', {
        scope: config.passport.vk.scopes ,
        //failureRedirect: '/auth'
    }),
    function (req, res) {
     // The request will be redirected to vk.com 
     // for authentication, so
     // this function will not be called.
     res.redirect('/');
    });
 
// router.get('/vk/callback', function (req, res, next) {

//   passport.authenticate('vk', function(err, user, info) {

//     console.log('vk/callback');
//     if (err) {
//       //return next(err); // will generate a 500 error
//       return res.send({ success : false, message : 'authentication failed' });
//     }
//     // Generate a JSON response reflecting authentication status
//     if (! user) {
//       return res.send({ success : false, message : 'authentication failed' });
//     }
//     return res.send({ success : true, message : 'authentication succeeded', user: user, info: info });
//   })(req, res, next);

// });

router.get('/vk/callback',
  passport.authenticate('vk', {
      failureRedirect: '/auth'
  }),
  function (req, res) {
      // Successful authentication
      //, redirect home.
      console.log('req.user', req.user);
      res.redirect('/cabinet/keys');
  });

router.get('/facebook',
    passport.authenticate('facebook', {
        scope: config.passport.facebook.scopes,
        //failureRedirect: '/auth'
    }),
    function (req, res) {
     // The request will be redirected to vk.com 
     // for authentication, so
     // this function will not be called.
     res.redirect('/');
    });

router.get('/facebook/callback',
  passport.authenticate('facebook', {
      failureRedirect: '/auth'
  }),
  function (req, res) {
      // Successful authentication
      //, redirect home.
      console.log('req.user', req.user);
      res.redirect('/cabinet/keys');
  });


router.get('/:service(vk|facebook)/delete', acl.ensureAccess,  function (req, res, next) {
  userService.destroyServiceByUser(req.user, req.params.service).then(function(logout) {
    if(logout){
        req.logout();
    }
    res.redirect('/');

  }).catch(function(err){
    
    res.send(500, { error: err });

  });
  
});



