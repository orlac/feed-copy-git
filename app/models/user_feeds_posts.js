var validateService = require('../validateService');


module.exports = function (sequelize, DataTypes) {

    var user = require('./user.js')(sequelize, DataTypes);
    var user_channel = require('./user_channel.js')(sequelize, DataTypes);
    var user_feeds = require('./user_feeds.js')(sequelize, DataTypes);

  var model = sequelize.define('user_feeds_posts', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty:{
                    msg: "пользователь не найден"
                }
            }
        },
        chid: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty:{
                    msg: "Выберите канал"
                },
                isChannelOfUser: function (value, next) {
                    validateService.isChannelOfUser(user_channel).apply(this, [value, next]);
                }    
            }
        },
        fid: {
            notEmpty:{
                msg: "Выберите пост"
            },
            type: DataTypes.INTEGER,
        },
        gid: {
            notEmpty:{
                msg: "Выберите группу"
            },
            type: DataTypes.INTEGER
        },
        type: {
            type:   DataTypes.ENUM,
            values: ['vk', 'facebook']
        },
        pid: {
            notEmpty:{
                msg: "Выберите id поста"
            },
            type: DataTypes.STRING,
        },

        
        
        deletedOn: {
            type: DataTypes.DATE,
            defaultValue: null,
        },
        createdAt: {
            type: DataTypes.DATE
        },
        updatedAt: {
            type: DataTypes.DATE
        }
    
    }, {
        timestamps: true,
        // freezeTableName: true,
        tableName: 'user_feeds_posts'
  });

  return model;
};