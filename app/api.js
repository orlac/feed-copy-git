var VK = require('vksdk');
var Facebook = require('facebook-node-sdk');
//var async = require('async-q');
var async = require('async');
var _ = require('lodash');
var Promise = require('promise');
var moment = require('moment'); 
var timeZoneService = require('./timeZoneService.js'); 
var _ = require('lodash');


var vkRequest = function(_vk, method, params){
	params = params || {};
	return new Promise(function (resolve, reject) {
		_vk.request(method, params, function(_o) {
		    if (_o.response) {
                resolve(_o.response);
            } else {
                reject(_o.error)
            }
		});
	});
}

var fbRequest = function(_fb, path, method, params){
	params = params || {};
	return new Promise(function (resolve, reject) {
		var apgs = [path, method, params, function(err, _o) {
		    if (err || _o.error) {
		    	reject( err || _o.error);
            } else {
                resolve(_o);
            }
		}];

		apgs = apgs.filter(function(val){
			return (val)? true: false;
		});

		_fb.graph.apply(_fb, apgs);
	});
}

module.exports = function(config){

	var s = function(){
		this.vk = new VK({
		   appId     : config.passport.vk.app_id,
		   appSecret : config.passport.vk.secret,
		   language  : 'ru',
		   version: '5.28'
		});

		this.facebook = new Facebook({ 
			appID: config.passport.facebook.app_id, 
			secret: config.passport.facebook.secret 
		});

		this.vkRequest = vkRequest;
		this.fbRequest = fbRequest;
	}
	
	return new s;



}