var express = require('express'),
  config = require('./config/config'),
  db = require('./app/models');


var passport = require('passport');

var app = express();

// var logger = require('./app/logger');
// logger.log(config);
// logger.logger. debug(config);

 
db.sequelize
  .sync()
  .then(function () {
    app.listen(config.port);
  }).catch(function (e) {
    throw new Error(e);
  });

require("./config/passport")(passport, config, app);
require('./config/express')(app, config, passport);  

