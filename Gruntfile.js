'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var reloadPort = 35729, files;

  // configurable paths
  var yeomanConfig = {
    www: 'public',
    app: 'app',
    dist: 'dist'
  };

  grunt.initConfig({
    yeoman: yeomanConfig,
    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'app.js'
      }
    },
    less: {
      dist: {
        files: {
          'public/css/style.css': 'public/css/style.less'
        }
      }
    },
    watch: {
      options: {
        nospawn: true,
        //livereload: reloadPort
      },
      // jsapp: {
      //   tasks: ['vulcanize', 'browserify']
      // },
      js_prod: {
        files: [
          'app.js'
        ],
        tasks: [
          'develop', 
          //'delayed-livereload'
        ]  
      },
      js: {
        files: [
          'app.js',
          'app/**/*.js',
          'config/*.js'
        ],
        tasks: [
          'develop', 
          //'delayed-livereload'
        ]
      },
      css: {
        files: [
          'public/css/*.less'
        ],
        tasks: ['less'],
        options: {
          //livereload: reloadPort
        }
      },
      elements: {
        files: [
          '!public/elements/elements.vulcanized.html',
          'public/elements/*.html',
          'public/elements/**/*.html',
          'public/elements/**/*.css',
          'public/elements/*.js',
          'public/elements/**/*.js',
          'public/css/*.css'
        ],
        tasks: ['vulcanize']
      },
      publicjs: {
        files: [
          '!public/jsapp/build.main.js',
          'public/jsapp/*.js',

        ],
        tasks: ['browserify'],
      },
      // views: {
      //   files: [
      //     'app/views/*.marko',
      //     'app/views/**/*.marko'
      //   ],
      //   options: { 
      //     //livereload: reloadPort
      //   }
      // }
      views: {
        files: [
          'app/views/*.ejs',
          'app/views/**/*.ejs'
        ],
        options: { 
          //livereload: reloadPort 
        }
      }
    },
    vulcanize: {
      default: {
        options: {
          //inline: true,
          //strip: true,
          strip: false,
          //'no-strip-excludes': true,
          inline: true,
          //,csp: '<%= yeoman.www %>/elements/elements.vulcanized.js'
        },
        files: {
          '<%= yeoman.www %>/elements/elements.vulcanized.html': [
            // '<%= yeoman.www %>/pages/*.html',
            // '<%= yeoman.www %>/pages/**/*.html',
            // '<%= yeoman.www %>/layouts/*',
            '<%= yeoman.www %>/elements/elements.html'
          ]
        }
      }
      
    },
    browserify: {
      default:{
        options: {
            browserifyOptions: {
                debug: true
              } 
          },
        src: '<%= yeoman.www %>/jsapp/main.js',
        dest: '<%= yeoman.www %>/jsapp/build.main.js',
      }
      // ,elements: {
      //   options: {
      //       browserifyOptions: {
      //           debug: true
      //         } 
      //     },
      //   src: '<%= yeoman.www %>/jsapp/elements.js',
      //   dest: '<%= yeoman.www %>/jsapp/build.elements.js',
      // }
    } 
  });

  grunt.config.requires('watch.js.files');
  files = grunt.config('watch.js.files');
  files = grunt.file.expand(files);

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function(err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded)
            grunt.log.ok('Delayed live reload successful.');
          else
            grunt.log.error('Unable to make a delayed live reload.');
          done(reloaded);
        });
    }, 500);
  });

  //todo watch and vulcanize

  grunt.registerTask('default', [
    'less',
    'vulcanize',
    'browserify',
    'develop',
    'watch'
  ]);

  grunt.registerTask('build', [
    'less',
    'vulcanize',
    'browserify',
    // 'develop',
    // 'watch:js_prod'
  ]);

};
