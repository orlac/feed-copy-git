(function(window){
  Polymer('upload-image', {
    
      dropText: 'Drop files here...',
      
      publish: {
        files: []  
      },

      render: function (src){
        var me = this;
        // var image = new Image();
        // image.onload = function(){
        //   var canvas = document.getElementById("myCanvas");
        //   if(image.height > MAX_HEIGHT) {
        //     image.width *= MAX_HEIGHT / image.height;
        //     image.height = MAX_HEIGHT;
        //   }
        //   var ctx = canvas.getContext("2d");
        //   ctx.clearRect(0, 0, canvas.width, canvas.height);
        //   canvas.width = image.width;
        //   canvas.height = image.height;
        //   ctx.drawImage(image, 0, 0, image.width, image.height);
        // };
        me.$.image.src = src;
      },

      loadImage: function(src){
        //  Prevent any non-image file type from being read.
        var me = this;
        if(!src.type.match(/image.*/)){
          console.log("The dropped file is not an image: ", src.type);
          return;
        }else{
          //  Create our FileReader and run the results through the render function.
          var reader = new FileReader();
          reader.onload = function(e){
            me.render(e.target.result);
          };
          reader.readAsDataURL(src);
        }
      },

      readURL: function () {
          var input = this.$.file;
          var me = this;
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                  me.render(e.target.result);
              }
              me.files = [];
              me.files.push(input.files[0]);
              reader.readAsDataURL(input.files[0]);
          }
      },

      ready: function(){
        var dropbox = this.$.dropbox;
        var filebox = this.$.file;
        var me = this;

        dropbox.addEventListener("click", function(e){
          filebox.click();
        });
        //============== DRAG & DROP =============
        // source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/
        

        // init event handlers
        function dragEnterLeave(evt) {
            evt.stopPropagation()
            evt.preventDefault()
            me.dropText = 'Drop files here...'
            me.dropClass = ''
        }
        dropbox.addEventListener("dragenter", dragEnterLeave, false)
        dropbox.addEventListener("dragleave", dragEnterLeave, false)
        dropbox.addEventListener("dragover", function(evt) {
            evt.stopPropagation()
            evt.preventDefault()
            var clazz = 'not-available'
            var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
            
            me.dropText = ok ? 'Drop files here...' : 'Only files are allowed!'
            me.dropClass = ok ? 'over' : 'not-available'
        
        }, false);


        dropbox.addEventListener("drop", function(evt) {
            console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
            evt.stopPropagation()
            evt.preventDefault()
            me.dropText = 'Drop files here...'
            me.dropClass = ''
            var files = evt.dataTransfer.files
            if (files.length > 0) {
                me.files = [];
                me.files.push(files[0]);
                me.loadImage(files[0]);
                
            }
        }, false)
        //============== DRAG & DROP =============

        // me.setFiles = function(element) {
        //   me.$apply(function(me) {
        //     console.log('files:', element.files);
        //     // Turn the FileList object into an Array
        //       me.files = []
        //       for (var i = 0; i < element.files.length; i++) {
        //         me.files.push(element.files[i])
        //       }
        //     me.progressVisible = false
        //   });
        // };

        // me.uploadFile = function() {
        //     var fd = new FormData()
        //     for (var i in me.files) {
        //         fd.append("uploadedFile", me.files[i])
        //     }
        //     var xhr = new XMLHttpRequest()
        //     xhr.upload.addEventListener("progress", uploadProgress, false)
        //     xhr.addEventListener("load", uploadComplete, false)
        //     xhr.addEventListener("error", uploadFailed, false)
        //     xhr.addEventListener("abort", uploadCanceled, false)
        //     xhr.open("POST", "/fileupload")
        //     me.progressVisible = true
        //     xhr.send(fd)
        // }

        // function uploadProgress(evt) {
        //     me.$apply(function(){
        //         if (evt.lengthComputable) {
        //             me.progress = Math.round(evt.loaded * 100 / evt.total)
        //         } else {
        //             me.progress = 'unable to compute'
        //         }
        //     })
        // }

        // function uploadComplete(evt) {
        //      This event is raised when the server send back a response 
        //     alert(evt.target.responseText)
        // }

        // function uploadFailed(evt) {
        //     alert("There was an error attempting to upload the file.")
        // }

        // function uploadCanceled(evt) {
        //     me.$apply(function(){
        //         me.progressVisible = false
        //     })
        //     alert("The upload has been canceled by the user or the browser dropped the connection.")
        // }

      }
      
    });
})(window);