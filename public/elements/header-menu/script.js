(function(scope){
    'use strict';

    var polymer = Polymer('header-menu', {
      /**
       * The `noevent` event is not actually fired from here,
       * we document it as an example of documenting events.
       *
       * @event noevent
       */

      /**
       * The `notitle` attribute does not yet have a purpose.
       *
       * @attribute notitle
       * @type string
       */
      notitle: '515646546',

      /**
       * The `aProp` is a property that does something cool.
       *
       * @property aProp
       * @type bool
       */
      aProp: false,

      ready: function() {
        console.log('ready', scope);
      },

      /**
       * The `task` method does no work at this time.
       *
       * @method task
       * @return {Object} Returns undefined.
       * @param {String} dummy Serves no purpose today.
       */
      task: function(dummy) {
        return dummy;
      }

    });

    // scope.scriptElements = scope.scriptElements || {};
    // scope.scriptElements.header_menu = polymer;
    return polymer;

})(window);