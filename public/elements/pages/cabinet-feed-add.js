(function(window){
	Polymer('cabinet-feed-add', {
		
  		selected: -1,
  		channels: [],
  		selectedChannel: null,
  		channelGroups: [],

  		//----------------
  		formData: {
  			//дата публикации
  			selectedDate: null,
  			//время публикации
  			selectedTime: null,
  			message: '',
  			//удалить пост
  			isDelete: false,
  			//удалить через
  			deleteAfter: null,
        timeZone: null,
  		},

  		pickerSettings: {
  			min: null,
  			max: null,
  			infiniteScrolling: false
  		},

      timeZoneList: [],

  		deleteData: [],
  		//----------------

  		observe: {
  			selectedId: 'selectedChannelChange'
  		},
  		publish: {
  			selectedId: null,
  		},

  		datePickerOpen: function(){
  			//this.$.datePicker.fire('selection-changed');
  			this.$.datePicker.open();
  		},

  		selectedChannelChange: function(){
  			this.buildListGroups(this.selectedId);
  		},

  		buildListGroups: function(channelId){
  			this.selectedChannel = _.find(this.channels, function(item){
  				return item.id == channelId;
  			});
  			this.channelGroups = (this.selectedChannel)? this.selectedChannel.groups : [];
  			this.channelGroups = _.map(this.channelGroups, function(item){
  				item.checked = true;
  				return item;
  			});
  		},

  		initPicker: function(){
  			var me = this
  			this.$.datePicker.min = this.pickerSettings.min;
  			this.$.datePicker.max = this.pickerSettings.max;
  			this.$.datePicker.infiniteScrolling = false;

  			me.formData.selectedDate = moment().format('DD.MM.YYYY');
  			this.$.datePicker.addEventListener('value-changed', function(e, detaill, target){
  				console.log('value-changed', e, detaill, target, this.value);
  				me.formData.selectedDate = moment(this.value).format('DD.MM.YYYY');
  			})
  		},

  		initTimePicker: function(){
  			var me = this
  			this.$.timePicker.time = window.moment().toDate();
  			this.$.timePicker.addEventListener('changed', function(e, detaill, target) {
  				me.formData.selectedTime = window.moment(this.time).format('H:mm');
			    console.log( 'initTimePicker', this, this.time, e, detaill, target);
			});
  		},

  		submit: function(){
  			var me = this;
  			var _groups = _.filter(this.channelGroups, function(item){
  				return item.checked == true;
  			});
  			var gids = _.map(_groups, function(item){
  				return {
  					id: item.id,
  					type: item.type
  				};
  			});

        // var _time = window.moment(this.$.timePicker.time).format('HH:mm');
        // var _date = window.moment(this.$.datePicker.value).format('DD.MM.YYYY');
        // var _d = [_date, _time, this.formData.timeZone ].join(' ');
        // var _public_date = window.moment( _d, 'DD.MM.YYYY HH:mm Z');
        // _public_date = _public_date.format('DD.MM.YYYY HH:mm Z');



        me.formData.selectedTime = window.moment(this.$.timePicker.time).format('HH:mm');
        var _data = window._.extend({}, me.formData);
        _data.gids = gids;
  			// _data.data = this.formData;
  			_data.cid = (this.selectedChannel)? this.selectedChannel.id : null;

  			this.$.api.saveUserFeed(this.$.image.files, _data, function(res){
  				
  				this.$.api.go('/cabinet/feed/list');
	    		this.$.notify.notify(res.message);

  			}.bind(this), function(err){
  				//todo отобразить ошибки на форме
          this.$.notify.error(err);
  			}.bind(this));
  		},

  		ready: function(){
  			console.log('cabinet-feed-add');
  			this.$.api.ensureAccess();
  			this.$.preload.loading=false;

  			this.$.preload.loading = true;
	    	this.$.api.getUserChannels(function(data){
  				this.channels = data.channels;
  				this.$.preload.loading = false;

          if(!this.selectedId && window._.first(this.channels)){
            this.selectedId = window._.first(this.channels).id;
          }

  				this.buildListGroups(this.selectedId);

  			}.bind(this));

	    	this.$.api.getPublishSettings(function(data){
  				this.pickerSettings.min = data.minDate;
  				this.pickerSettings.max = data.maxDate;
          this.deleteData = data.forDelete;
          // this.formData.timeZone = data.defaultTimeZone;
  				// this.timeZoneList = data.timeZoneList;

  				this.initPicker();
  				this.initTimePicker();
  			}.bind(this));
  		}
  	});
})(window);