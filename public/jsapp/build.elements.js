(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function(scope){
    'use strict';

    var polymer = Polymer('header-menu', {
      /**
       * The `noevent` event is not actually fired from here,
       * we document it as an example of documenting events.
       *
       * @event noevent
       */

      /**
       * The `notitle` attribute does not yet have a purpose.
       *
       * @attribute notitle
       * @type string
       */
      notitle: '515646546',

      /**
       * The `aProp` is a property that does something cool.
       *
       * @property aProp
       * @type bool
       */
      aProp: false,

      ready: function() {
        console.log('ready', scope);
      },

      /**
       * The `task` method does no work at this time.
       *
       * @method task
       * @return {Object} Returns undefined.
       * @param {String} dummy Serves no purpose today.
       */
      task: function(dummy) {
        return dummy;
      }

    });

    // scope.scriptElements = scope.scriptElements || {};
    // scope.scriptElements.header_menu = polymer;
    return polymer;

})(window);
},{}],2:[function(require,module,exports){
require('../elements/header-menu/script.js');
},{"../elements/header-menu/script.js":1}]},{},[2])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlc1xcZ3J1bnQtYnJvd3NlcmlmeVxcbm9kZV9tb2R1bGVzXFxicm93c2VyaWZ5XFxub2RlX21vZHVsZXNcXGJyb3dzZXItcGFja1xcX3ByZWx1ZGUuanMiLCJwdWJsaWMvZWxlbWVudHMvaGVhZGVyLW1lbnUvc2NyaXB0LmpzIiwicHVibGljL2pzYXBwL2VsZW1lbnRzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaERBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIihmdW5jdGlvbihzY29wZSl7XHJcbiAgICAndXNlIHN0cmljdCc7XHJcblxyXG4gICAgdmFyIHBvbHltZXIgPSBQb2x5bWVyKCdoZWFkZXItbWVudScsIHtcclxuICAgICAgLyoqXHJcbiAgICAgICAqIFRoZSBgbm9ldmVudGAgZXZlbnQgaXMgbm90IGFjdHVhbGx5IGZpcmVkIGZyb20gaGVyZSxcclxuICAgICAgICogd2UgZG9jdW1lbnQgaXQgYXMgYW4gZXhhbXBsZSBvZiBkb2N1bWVudGluZyBldmVudHMuXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBldmVudCBub2V2ZW50XHJcbiAgICAgICAqL1xyXG5cclxuICAgICAgLyoqXHJcbiAgICAgICAqIFRoZSBgbm90aXRsZWAgYXR0cmlidXRlIGRvZXMgbm90IHlldCBoYXZlIGEgcHVycG9zZS5cclxuICAgICAgICpcclxuICAgICAgICogQGF0dHJpYnV0ZSBub3RpdGxlXHJcbiAgICAgICAqIEB0eXBlIHN0cmluZ1xyXG4gICAgICAgKi9cclxuICAgICAgbm90aXRsZTogJzUxNTY0NjU0NicsXHJcblxyXG4gICAgICAvKipcclxuICAgICAgICogVGhlIGBhUHJvcGAgaXMgYSBwcm9wZXJ0eSB0aGF0IGRvZXMgc29tZXRoaW5nIGNvb2wuXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBwcm9wZXJ0eSBhUHJvcFxyXG4gICAgICAgKiBAdHlwZSBib29sXHJcbiAgICAgICAqL1xyXG4gICAgICBhUHJvcDogZmFsc2UsXHJcblxyXG4gICAgICByZWFkeTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ3JlYWR5Jywgc2NvcGUpO1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgLyoqXHJcbiAgICAgICAqIFRoZSBgdGFza2AgbWV0aG9kIGRvZXMgbm8gd29yayBhdCB0aGlzIHRpbWUuXHJcbiAgICAgICAqXHJcbiAgICAgICAqIEBtZXRob2QgdGFza1xyXG4gICAgICAgKiBAcmV0dXJuIHtPYmplY3R9IFJldHVybnMgdW5kZWZpbmVkLlxyXG4gICAgICAgKiBAcGFyYW0ge1N0cmluZ30gZHVtbXkgU2VydmVzIG5vIHB1cnBvc2UgdG9kYXkuXHJcbiAgICAgICAqL1xyXG4gICAgICB0YXNrOiBmdW5jdGlvbihkdW1teSkge1xyXG4gICAgICAgIHJldHVybiBkdW1teTtcclxuICAgICAgfVxyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIC8vIHNjb3BlLnNjcmlwdEVsZW1lbnRzID0gc2NvcGUuc2NyaXB0RWxlbWVudHMgfHwge307XHJcbiAgICAvLyBzY29wZS5zY3JpcHRFbGVtZW50cy5oZWFkZXJfbWVudSA9IHBvbHltZXI7XHJcbiAgICByZXR1cm4gcG9seW1lcjtcclxuXHJcbn0pKHdpbmRvdyk7IiwicmVxdWlyZSgnLi4vZWxlbWVudHMvaGVhZGVyLW1lbnUvc2NyaXB0LmpzJyk7Il19
